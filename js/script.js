$(function() {


  $('.tabs-nav a').click(function() {

    // Adding active class 

    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');

    // Display active tab

    let activeTab = $(this).attr('href');
    $('.tabs-item').hide();
    $(activeTab).show();

    return false;
  });


    $('.tabs-nav a:first').trigger('click');
});